FROM node:14-alpine

USER root

WORKDIR /app

RUN chmod -R 777 /app

COPY package.json package-lock.json* ./
RUN npm install

COPY . .

CMD ["npm", "test"]
